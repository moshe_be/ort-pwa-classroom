import React, {useRef, useCallback} from 'react';
import classes from './newsletter.css';
import {gql, useMutation} from '@apollo/client';

const Newsletter = (props) => {

    const [subscribeToNewsletter, { loading, data, error, called }] = useMutation(SUB_TO_NEWSLETTER);
    const inputRef = useRef();

    const handleSubscribe = useCallback(
        async () => {
        if(!inputRef.current.value) {
            return;
        }
        try {
            await subscribeToNewsletter({
                variables : {
                    email : inputRef.current.value
                }
            });
        } catch (e) {
            //not needed in this case, 'error' variable will be updated properly after data returns.
        }
    });

    return (
        <>
            <div className={classes.container}>
                <h4>Subscribe to our newsletter!</h4>
                <form>
                    <input type="text" placeholder="email" ref={inputRef} ></input>
                    <button type="button" onClick={handleSubscribe}>Subscribe</button>
                </form>
                {called && !loading && !error && <div>
                    <p className={classes.message}> You subscribed to the newsletter successfully!</p>
                </div>}
                {error && <div>
                    <p className={classes.error}>{error.message}</p>
                </div>}
            </div>
        </>
    )
}

export default Newsletter;

export const SUB_TO_NEWSLETTER = gql(`
    mutation MySubToNewsletter($email : String!){
        subscribeEmailToNewsletter(email : $email) {
        status
        }
    }
`); 