import React, { Fragment } from 'react';
import {gql, useQuery} from '@apollo/client';
import Newsletter from '../newsletter/newsletter';

const Homepage = (props) => {

    const {error, data, loading} = useQuery(GET_MY_PRODUCTS_MOD, {
        fetchPolicy : 'cache-and-network', 
        variables : {
          numOfProducts:2,
          searchTerm: 'pants'
        }
    });



    if(error) {
        return (
            <div>
                An Error has occured!
            </div>
        );
    }
    if(loading || !data) {
        return (
            <div>
                Loading...
            </div>
        )
    }


    return (
    <Fragment>
    <section id="products-display">
      <div>
        {data.products.items.map(product => {
            return (
                <div>
                    {product.name}, {product.price.regularPrice.amount.value}
                </div>
            )
        })}
      </div>
    </section>
    <section id="newsletter" >
      <Newsletter />
    </section>
    </Fragment>
    )
}

export default Homepage;

export const GET_MY_PRODUCTS = gql(`
{
    products(filter:{
      price :{
        from :"10",
        to : "25"
      }
    }) {
      items {
        name
        id
        small_image {
          url
        }
        url_key
        price {
                  regularPrice {
            amount {
              value
            }
          }
        }
      }
    }
  }
`);

export const GET_MY_PRODUCTS_MOD = gql(`
query MyGetProducts(
  $numOfProducts : Int = 6,
	$searchTerm : String = "",
  $currentPage : Int = 1
) {
  products( 
    pageSize : $numOfProducts,
    currentPage : $currentPage,
    search : $searchTerm
  ) {
    items {
      name
      price {
        regularPrice {
          amount {
            value
          }
        }
      }
    }
    total_count
    page_info {
      current_page
      total_pages
    }
  }
}
`);